#include<stdio.h> 
#include<string.h> 
#include<arpa/inet.h> 


/*
BLAB -  Bind (enlace al puerto)
	Listen
	Aceppt (espera a que un cliente se conecte)
	Begin to talk (empezar a hablar)
2 sockets - 1 listen y 1 response
*/

void bind_to_port(int socket, int port){
	struct sockaddr_in name;
	name.sin_family	= PF_INET;
	name.sin_port = (in_port_t)htons(port);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	
	int reuse = 1;
	if (setsockopt(socket,SOL_SOCKET,SO_REUSEADDR,(char*)&reuse,sizeof(int)) == -1){error("No es posible reusar el sockets\n");
	}
	int c = bind(socket, (struct sockaddr*) &name, sizeof(name));
	if(c==-1){
	perror("No es posible enlazar al puerto : direccion ya esta en uso \n");	
	}
}

int main(){
const int PORT = 7200;
char * advice[]={"Deberias salir hoy\n\r",
		"Deberias salir hoy\n\r",
		"Deberias salir hoy\n\r",
		"Deberias salir hoy\n\r",
		"Una palabra: inapropiado\n\r"
		};
	int listener = open_socket();

	if (listener == -1){
		printf("Error en el listener \n");
		return;
	}

	bind_to_port(listener, PORT);

	if (listen (listener,10) == -1){
		printf("No es posible escuchar en ese puerto \n");
		return;
	}
	
	printf("Esta enlazado al puerto\n");
	while(1){ 
		struct sockaddr_storage client ;
		unsigned int addres_size = sizeof(client);
		int connect = accept(listener, (struct sockaddr*) &client, &addres_size);
		if(connect == -1){
		printf("No se puede conectar socket secundario \n");		
		}
		printf("Atendiendo al cliente \n");
		char* msg = advice[rand() %5];
		send(connect,msg,strlen(msg),0);
		msg=NULL;
		close(connect);
	}

return 0;
}

/* Implementacion del socket*/

int open_socket(){
	int s = socket(PF_INET,SOCK_STREAM,0);
	if(s == -1)
		printf("Error al abrir socket\n");
	return s;
}

