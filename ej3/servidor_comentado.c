#include<stdio.h> // imprimir en terminal (estandar de E/S)
#include<string.h> // sacar longitudes de las cadenas
#include<arpa/inet.h> // nos asegura que podemos ocupar las llamadas al sistema para implementar el socket


/*
BLAB -  Bind (enlace al puerto)
	Listen
	Aceppt (espera a que un cliente se conecte)
	Begin to talk (empezar a hablar)
2 sockets - 1 listen y 1 response
*/

int main(){
const int PORT = 7200;
char * advice[]={"Deberias salir hoy\n\r",
		"Una palabra: inapropiado\n\r"
		};
	int listener = open_socket(); //abrimos el socket que v aa estar a la escucha de los clientes que se vana  estar conectando 
	if (listener == -1){
		printf("Error en el listener \n");
		return;
	}

	bind_to_port(listener,PORT); //Enlace al puerto, le paso el listener y el port Hay 65536 puertos del 0-1024 ocupados
	if (listener (listener,10) == -1){ // a partir del cliente 11 nos dara este mensaje
		printf("No es posible escuchar en ese puerto \n");
		return;
	}
	
	printf("Esta enlazado al puerto\n");
	while(1){ 
		struct sockaddr_storage client ; // nos permite guardar la info del cliente que se esta conectando
		unsigned int addres_size = sizeof(client);
		int connect = accept(listener, (struct sockaddr*) &client, &addres_size); // bloquea aplicacion ala espera de cualqueir cliente que se quiera conectar
		if(connect == -1){
		printf("No se puede conectar socket secundario \n");		
		}
		printf("Atendiendo al cliente \n");
		char* msg = advice[rand() %5];
		send(connect,msg,strlen(msg),0); // puede sed sens write o cualquier otro
		msg=NULL;
		close(connect);
	}

return 0;
}

/* Implementacion del socket*/

int open_socket(){
	int s = socket(PF_INET,SOCK_STREAM,0); // (PF_INET:dominio 2 computadoras en distintos lugares,SOCK_STREAM: vamos a utilizar un flujo de datos que se va a enviar en la red,0:protocolo, se encarga el SO)
	if(s == -1) // si hay error devuelve -1
		printf("Error al abrir socket\n");
	return s;
}

void bind_to_port(int socket, int port){
	struct sockaddr_in name; // toda la info del puerto y tipo de dominio
	name.sin_family		= PF_NET;
	name.sin_port		= (in_port_t)htons(port);
	name.sin_addr.s_addr= htonl(INADDR_ANY);
	
	int reuse = 1;
	inf (setsockopt(socket,SOL_SOCKET,SO_REUSEADDR,(char*)&reuse,sizeof(int)) == -1){error("No es posible reusar el sockets\n");
	} // para que peuda reusar el socket despues que lo usa
	int c = bind(socket, (struct sockaddr*) &name, sizeof(name));
	if(c==-1){
	perror("No es posible enlazar al puerto : direccion ya esta en uso \n");	
	}
